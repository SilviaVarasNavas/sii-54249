//Autora: Silvia Varas Navas
#pragma once

#include "DatosMemCompartida.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <iostream>

int main(void)
{
	DatosMemCompartida *datosm;
	char *org;
	int fdmem;
	
	fdmem=open("/tmp/DatosComp.txt", O_RDWR);

	//Proyectamos el fichero abierto en memoria
	org=(char*)mmap(NULL,sizeof(*(datosm)), PROT_WRITE|PROT_READ,MAP_SHARED, fdmem, 0); 
	close(fdmem);
	datosm = (DatosMemCompartida *) org;
	
	//Entramos en el bucle infinito dónde bot decide que hacer
	while (1){		
		float posicion_raqueta1_y;
		float posicion_raqueta1_x;
		posicion_raqueta1_y=(datosm->raqueta1.y2+datosm->raqueta1.y1)/2;
		posicion_raqueta1_x=(datosm->raqueta1.x1);
		float posicion_raqueta2_y;
		float posicion_raqueta2_x;
		posicion_raqueta2_y=(datosm->raqueta2.y2+datosm->raqueta2.y1)/2;
		posicion_raqueta2_x=(datosm->raqueta2.x1);

		//Programacion de la raqueta izquierda como bot
		if((datosm->esfera.centro.x<0)&&(datosm->esfera.velocidad.x<0))
		{
			if(posicion_raqueta1_y<datosm->esfera.centro.y)
				datosm->accion=1;
			else if(posicion_raqueta1_y>datosm->esfera.centro.y)
				datosm->accion=-1;
			else
				datosm->accion=0;
			usleep(25000);
		}
		//Programacion de la raqueta derecha como bot
	/*if((datosm->esfera.centro.x>0)&&(datosm->esfera.velocidad.x>0))
	{
		if(posicion_raqueta2_y<datosm->esfera.centro.y)
			datosm->accion2=1;
		else if(posicion_raqueta2_y>datosm->esfera.centro.y)
			datosm->accion2=-1;
		else
			datosm->accion2=0;
		usleep(25000);
	}*/
	}
	munmap(org,sizeof(*(datosm)));
}
