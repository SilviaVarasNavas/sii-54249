INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(COMMON_SRCS_CLIENTE  
	MundoCliente.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp)

SET(COMMON_SRCS_SERVIDOR  
        MundoServidor.cpp 
        Esfera.cpp
        Plano.cpp
        Raqueta.cpp
        Vector2D.cpp	
	Socket.cpp)
		
ADD_EXECUTABLE(Cliente Cliente.cpp ${COMMON_SRCS_CLIENTE})
ADD_EXECUTABLE(Servidor Servidor.cpp ${COMMON_SRCS_SERVIDOR})
ADD_EXECUTABLE(logger logger.cpp)
ADD_EXECUTABLE(bot bot.cpp)

TARGET_LINK_LIBRARIES(Servidor glut GL GLU -lpthread)
TARGET_LINK_LIBRARIES(Cliente glut GL GLU -lpthread)

